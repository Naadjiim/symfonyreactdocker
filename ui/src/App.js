import './App.css';
import React from 'react';
import axios from 'axios';


function App() {
    const [data, setData] = React.useState('');


    const loadData = async () => {
        const response = await axios.get(process.env.REACT_APP_API_URL + '/healthCheck');
        setData(response.data);
    };

    React.useEffect(() => {
        loadData();
    }, []);

    return (
        <div className="App">
            <header className="App-header">
              Server HealthCheck status: {data.status}
            </header>
        </div>
    );
};

export default App;